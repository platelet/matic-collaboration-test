"user strict";

var express = require('express');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var Util = require('./util.js');
var Controls = require('./controls.js');

const PLAYER_CAP = 8;
const MIN_PLAYERS = 2;
const CUR_PLAYERS = 0;

let players = {};
let game = {
  stepsComplete: 0,
  controls: [],
  playerControls: {
    //id : array of controls
  },
  playerMessages: {
    //id: message
  }
};

function sendUserInfo() {
  io.emit('USER_INFO', Util.objToArray(players));
}
function sendObjectiveUpdate() {
  io.emit('OBJECTIVE_UPDATE', Util.objToArray(players));
}

function checkPlayersReady() {
  let playersReady = true;
  let keys = Object.keys(players);
  if (keys.length < MIN_PLAYERS) return false;
  keys.forEach((e, i) => {
    if (!players[e].readyState) playersReady = false;
  });
  return playersReady;
}

function setPlayerObjective(player) {
  let objective = game.controls[Math.floor(Math.random()*game.controls.length)];
  player.objective = objective;
  return player;
}

function initGame() {
  let game_controls = [];
  let used_controls = [];
  Object.keys(players).forEach((_id, i) => {
    let player = players[_id];
    let p_controls = [];
    while (p_controls.length !== 4) {
      let n_control = Controls[Math.floor(Math.random()*Controls.length)];
      if (used_controls.indexOf(n_control.question) === -1) {
        p_controls.push(n_control);
        used_controls.push(n_control.question);
        game_controls.push(n_control);
      }
    }
    players[_id].controls = p_controls;
  });
  game.controls = game_controls;

  //set player objectives
  Object.keys(players).forEach((_id, i) => {
    players[_id] = setPlayerObjective(players[_id]);
  });

  io.emit('GAME_INIT', Util.objToArray(players));
}

io.on('connection', socket => {
  console.log('a user connected', socket.id);
  players[socket.id] = {
    _id: socket.id,
    readyState: false,
    controls: [],
    objective: false
  };

  sendUserInfo();

  socket.on('PLAYER_READY', data => {
    console.log(socket.id,'is ready');
    players[socket.id].readyState = true;
    sendUserInfo();
    if (checkPlayersReady()) {
      initGame();
    }
  });

  socket.on('SET_NICKNAME', data => {
    console.log(socket.id,'sets nickname to',data);
    players[socket.id].nickname = data;
    sendUserInfo();
  });

  socket.on('OBJECTIVE_CLICK', data => {
    Object.keys(players).forEach((_id, i) => {
      let obj = players[_id].objective;
      if (obj.answer === data.answer) {
        players[_id] = setPlayerObjective(players[_id]);
        sendObjectiveUpdate();
      }
    });
  });

  socket.on('disconnecting', (reason) => {
    console.log('user disconnecting', socket.id);
    delete players[socket.id];
  });
});

/**** ROUTER START ****/
app.use(express.static('public'));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});


/*** SERVER START ***/
let port = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 8080;
let ip = process.env.OPENSHIFT_NODEJS_IP || process.env.IP || '0.0.0.0';

console.log(process.env, process.env.OPENSHIFT_NODEJS_IP);

http.listen(port, ip, function(){
  console.log('listening on',ip,':',port);
});
