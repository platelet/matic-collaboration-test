"use strict";

let Util = {
  objToArray: (o) => {
    let arr = [];
    Object.keys(o).forEach((e, i) => {
      let el = o[e];
      arr.push(el);
    });
    return arr;
  }
}

module.exports = Util;
