let Colors = require('./colors');

const Controls = [
  {
    question: '1 + 1',
    answer: 2,
    color: Colors.green
  },
  {
    question: '4 x 5',
    answer: 20,
    color: Colors.green
  },
  {
    question: '2 x 5',
    answer: 10,
    color: Colors.green
  },
  {
    question: '3 x 5',
    answer: 15,
    color: Colors.green
  },
  {
    question: '1 x 5',
    answer: 5,
    color: Colors.green
  },
  {
    question: '5 x 5',
    answer: 25,
    color: Colors.green
  },
  {
    question: '6 x 5',
    answer: 30,
    color: Colors.green
  },
  {
    question: '7 x 5',
    answer: 35,
    color: Colors.green
  },
  {
    question: '8 x 5',
    answer: 40,
    color: Colors.green
  },
  {
    question: '9 x 5',
    answer: 45,
    color: Colors.green
  },
  {
    question: '10 x 5',
    answer: 50,
    color: Colors.green
  },
  {
    question: '11 x 5',
    answer: 55,
    color: Colors.green
  },
  {
    question: '12 x 5',
    answer: 60,
    color: Colors.green
  },
  {
    question: '1 + 9',
    answer: 10,
    color: Colors.red
  },
  {
    question: '2 + 9',
    answer: 11,
    color: Colors.red
  },
  {
    question: '3 + 9',
    answer: 12,
    color: Colors.red
  },
  {
    question: '4 + 9',
    answer: 13,
    color: Colors.red
  },
  {
    question: '5 + 9',
    answer: 14,
    color: Colors.red
  },
  {
    question: '6 + 9',
    answer: 15,
    color: Colors.red
  },
  {
    question: '7 + 9',
    answer: 16,
    color: Colors.red
  },
  {
    question: '8 + 9',
    answer: 17,
    color: Colors.red
  },
  {
    question: '9 + 9',
    answer: 18,
    color: Colors.red
  },
  {
    question: '10 + 9',
    answer: 19,
    color: Colors.red
  }
];

module.exports = Controls;
