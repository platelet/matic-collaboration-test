window.game = {
  socket: null,
  me: {
    id: null,
    readyState: false
  },
  players: []
};

function updateWaitingScreen() {
  var me_s = (window.game.me.readyState) ? ' (ready)' : ' (not ready)';
  $('p#waiting_me').text('You: '+window.game.me.nickname+' '+window.game.me._id+me_s);

  $('p.waiting_other').remove();
  var to_add = [];
  window.game.players.forEach(player => {
    var other_s = (player.readyState) ? ' (ready)' : ' (not ready)';
    to_add.push($('<p>').addClass('waiting_other').text(player.nickname+' '+player._id+other_s));
  });
  $('#waiting_me').after(to_add);
}

function updatePlayground() {
  $('#playground h1').text(window.game.me.objective.question);
  $('#playground button:eq(0)').addClass(window.game.me.controls[0].color).text(window.game.me.controls[0].answer);
  $('#playground button:eq(1)').addClass(window.game.me.controls[1].color).text(window.game.me.controls[1].answer);
  $('#playground button:eq(2)').addClass(window.game.me.controls[2].color).text(window.game.me.controls[2].answer);
  $('#playground button:eq(3)').addClass(window.game.me.controls[3].color).text(window.game.me.controls[3].answer);
}

function bindSocketEvents(socket) {
  socket.on('USER_INFO', function(data) {
    console.log('IO EVENT | USER_INFO |',data);
    window.game.players = [];
    data.forEach((e, i) => {
      if (e._id === window.game.socket.id)
        window.game.me = e;
      else
        window.game.players.push(e);
    });
    updateWaitingScreen();
  });

  socket.on('GAME_INIT', function (data) {
    console.log('IO EVENT | GAME_INIT |',data);
    window.game.players = [];
    data.forEach((e, i) => {
      if (e._id === window.game.socket.id)
        window.game.me = e;
      else
        window.game.players.push(e);
    });
    updatePlayground();
    changeView('playground');
  });

  socket.on('OBJECTIVE_UPDATE', function (data) {
    data.forEach((e, i) => {
      if (e._id === window.game.socket.id)
        window.game.me = e;
    });
    updatePlayground();
  });
}

function processAction(event) {
  let index = parseInt($(event.target).attr('data-index'));
  let control = window.game.me.controls[index];
  window.game.socket.emit('OBJECTIVE_CLICK', control);
}

function playerJoin() {
  changeView('working');
  window.game.socket = io();
  bindSocketEvents(window.game.socket);
  window.game.socket.on('connect', () => {
    window.game.socket.emit('SET_NICKNAME', $('#me_nickname').val())
    changeView('waiting');
  });
}

function playerReady() {
  $('#ready_button').hide();
  window.game.socket.emit('PLAYER_READY', true);
}

function changeView(newView) {
  $('section').hide();
  $('section#'+newView).show();
}
